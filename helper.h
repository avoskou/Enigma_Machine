
#include <iostream>
#include <fstream>
using namespace std;

/***********general purpose helper functions*******************/

//opens a file ,return error if fail
void open_file(ifstream &in_stream,char* in_file);

//checks if 0<=digit<=25,returns error if not
void invalid_index(int digit,char* file,const char* object);

//get a digit from a file ,returns error if is not integer
bool scan_numeric(ifstream &in_stream,int &digit,char* rotor_file,const char* object);




/**********used in main*****************************/

// return error if argc has not possible value 
void insufficient_number_of_parameters_check(int argc);
  



