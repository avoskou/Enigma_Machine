#include <iostream>
#include <fstream>
#include <string>
#include "enigma.h"
#include "errors.h"
#include "helper.h"

using namespace std;



int main(int argc,char** argv)
{
  char digit_char,in[512];
  int digit,i=0;
  
  try{

    insufficient_number_of_parameters_check(argc);

    Enigma machine(argc,argv);      

    cin.getline(in,512);
    
    for(i=0;in[i]!='\0';i++ ){

      if(in[i]<='Z'&& in[i]>='A'){
	digit=(int)(in[i]-65);        // char to int
	digit=machine.encrypt(digit); //mapping procedure
	digit_char=digit+'A';         // int back to char
	cout<<digit_char;
       }
      
      else{
	if(in[i]!=' ') {
	  cerr<<in[i]<<" is not a valid input character (input characters must be upper case letters A-Z)!"<<endl;
      	  throw INVALID_INPUT_CHARACTER;
	}
      }
      
    }
    
  }
  catch(const int error){
    return error;
  }
  

  return NO_ERROR;
}
