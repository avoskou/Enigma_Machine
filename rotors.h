#include <iostream>
#include <fstream>
using namespace std;
class Rotor{
 private :
   int wiring_fw[26];   // mapping wiring right to left      
   int wiring_bw[26];   // maps wiring left to right
   bool notch[26];       // e.g notch[7]=1 means there is a notch at 7     	
   int rot;              // total rotations made includin initial position
   Rotor *left_rotor ;   // the left "neighbor" rotor 
   
   //sets the mapping of the rotor
   void setup_mapping(ifstream &in_stream,char* rotor_file);

   //sets the notches of the rotor
   void setup_notch(ifstream &in_stream,char* rotor_file);

   //checks if mapping is valid   
   void invalid_rotor_mapping_multy(bool check[],int i,char* rotor_file);
  
   //checks if a notch inserted for than once
   void multiple_notch(bool notch[],int i,char*rotor_file);
   
   //return error if there are less than 26 mapping in the configuaration file 
   void invalid_rotor_mapping_notall(ifstream &in_stream,int &digit,int i,char* rotor_file);

 public:

   // setup the parameters of the rotor -  not the far left 
   void setup(char* rotor_file,int initial_pos,Rotor& left);
   
   // setup the parameters of the rotor - the far left
   void setup(char* rotor_file,int initial_pos);
   
   //rotate once the rotor and if notch hitted the rotor left 
   void rotate();
   
   // maps right to left
   void map_fw(int &digit);
   
   // maps left to right
   void map_bw(int &digit);
   
  
   
};
