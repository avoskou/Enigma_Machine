#include "errors.h"
#include <iostream>
#include <fstream>
#include "reflector.h"
#include "helper.h"

using namespace std;

void parameters_num_check(const bool contition){
  if(contition){
    cerr<<"Insufficient number of mappings in reflector file: reflector.rf ";
    throw INCORRECT_NUMBER_OF_REFLECTOR_PARAMETERS;
  }
}
     
void reflecotr_mapping_check(int temp1,int temp2,bool check[]){   
  if(temp1==temp2 || check[temp1] || check[temp2]){
    cerr<<"invalid reflector mapping";
    throw INVALID_REFLECTOR_MAPPING;
  }
}


Reflector::Reflector(char* reflector_file){
  int temp1,temp2;
  ifstream in_stream;
  bool check[26]={0};    // true if the digit is connected
  open_file(in_stream,reflector_file);
  for(int i=0;i<=12;i++){

    //read and error check for the first part of the pair 
    scan_numeric(in_stream,temp1,reflector_file,"reflector");
    invalid_index(temp1,reflector_file,"reflector");
    parameters_num_check(in_stream.eof());

    //read and error check the second part of the pair
    scan_numeric(in_stream,temp2,reflector_file,"reflector");
    invalid_index(temp2,reflector_file,"reflector");
    parameters_num_check(in_stream.eof());
     
    reflecotr_mapping_check(temp1,temp2,check);
    
    wiring[temp1]=temp2;  // wiring each digit to its pai 
    wiring[temp2]=temp1;  // wirning each digit to its pair
    
    check[temp1]=1;       // temp1  digit is connected
    check[temp2]=1;       // temp2  .....
  }
  in_stream>>temp1;
  parameters_num_check(!in_stream.eof());
  in_stream.close();
}

void Reflector::reflect(int &digit){
  digit=wiring[digit];
}  

