#include<fstream>
using namespace std;
class Plugboard{
  private :
 	// store maping wiring x -> wiring[x]  (if there is no given wiring for a digit y  wiring[y]=y
        int wiring[26];
   
	/*read a digit from file returns 1 for succes and 0 if there is no more digit to read
	returns error if the total number of digits was odd ,or if a digit isnt int ,or if  digit<0 or digit>25*/                       
        bool read_digit(ifstream &in_stream,int &temp,int counter,char* plugboard_file);
  public:
        // constract plugboard using wiring given in configuration file
   	Plugboard (char* plugboard_file);
        
        //maps digit using the given wiring
    	void map(int &digit);
};


