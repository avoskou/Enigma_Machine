#include <iostream>
#include <fstream>
#include <string>
#include "errors.h"
#include "rotors.h"
#include "helper.h"

using namespace std;



void Rotor::invalid_rotor_mapping_notall(ifstream &in_stream,int &digit,int i,char* rotor_file){
  if( in_stream.eof() && i<26 ){
    	cerr<<"Not all inputs mapped in rotor file: "<<rotor_file<<endl;
	throw INVALID_ROTOR_MAPPING;
  }
}

void Rotor::invalid_rotor_mapping_multy(bool check[],int i,char* rotor_file){

  if(check[wiring_fw[i]]==1){
    cerr<<"Invalid mapping of input "<<i<<" to output "<<wiring_fw[i]<<" (output "<<wiring_fw[i]<<" is already mapped to from input "<<wiring_bw[wiring_fw[i]]<<")"<<endl;
    throw INVALID_ROTOR_MAPPING;
  }
}
void Rotor::multiple_notch(bool notch[],int i,char* rotor_file){

  if(notch[i]==1){
    cerr<<"notch on "<<i<<"has been inserted more than once"<<endl;
    throw INVALID_ROTOR_MAPPING;
  }
}

void Rotor::setup_mapping(ifstream &in_stream,char* rotor_file){
  bool check[26]={0};
  for(int i=0;i<=25;i++){

    scan_numeric(in_stream,wiring_fw[i],rotor_file,"rotor");
    invalid_rotor_mapping_notall(in_stream,wiring_fw[i],i,rotor_file);
    invalid_index(wiring_fw[i],rotor_file,"rotor");
    invalid_rotor_mapping_multy(check,i,rotor_file);
    wiring_bw[wiring_fw[i]]=i;  
    check[wiring_fw[i]]=1;

  }
  
}


void Rotor::setup_notch(ifstream &in_stream,char* rotor_file ){
  int temp;
  for(int i=0;i<=25;i++)
    notch[i]=0;                                        //initialise each  notch array to 0 (no notches)
  
  while(scan_numeric(in_stream,temp,rotor_file,"rotor")){
    multiple_notch(notch,temp,rotor_file);
    invalid_index(temp,rotor_file,"rotor");
    notch[temp]=1;
  }
  
  scan_numeric(in_stream,temp,rotor_file,"rotor");

}

// setup rotor- not the far left  
void Rotor::setup(char* rotor_file,int initial_pos,Rotor& left){
  ifstream in_stream;
  open_file(in_stream,rotor_file);
  rot=initial_pos;
  left_rotor=&left;
  setup_mapping(in_stream,rotor_file);
  setup_notch(in_stream,rotor_file);
  in_stream.close();
  
}

// setup rotor - the very left
void Rotor::setup(char* rotor_file,int initial_pos){
  ifstream in_stream;
  open_file(in_stream,rotor_file);
  rot=initial_pos;
  left_rotor=NULL;
  setup_mapping(in_stream,rotor_file);
  setup_notch(in_stream,rotor_file);
  in_stream.close();
}

void Rotor::rotate(){
  rot++;
  rot=(rot%26);
  if(notch[rot])
    left_rotor->rotate();
}  

void Rotor::map_fw(int &digit){
  digit=(digit+rot)%26;
  digit=wiring_fw[digit];
  digit=(26+digit-rot)%26;
}   

void Rotor::map_bw(int &digit){
  digit=(digit+rot)%26;
  digit=wiring_bw[digit];
  digit=(26+digit-rot)%26;
}
