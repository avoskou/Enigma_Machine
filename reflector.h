
class Reflector{
  private:
   int wiring[26];                           //the reflection of each digit  x-> wiring[x]   
  public:
    
   //set wiring given in the configuration file
   Reflector(char* reflector_file);
  
   //maps each digit to its reflection  
   void reflect(int &digit);
};
