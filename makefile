enigma: main.o enigma.o rotors.o reflector.o plugboard.o helper.o
	g++ -g main.o enigma.o rotors.o reflector.o plugboard.o helper.o -o enigma 
main.o: main.cpp enigma.h 
	g++ -Wall -g -c main.cpp
enigma.o: enigma.cpp enigma.h  
	g++ -Wall -g -c enigma.cpp 
rotors.o: rotors.cpp rotors.h
	g++ -Wall -g -c rotors.cpp
reflector.o: reflector.cpp reflector.h
	g++ -Wall -g -c reflector.cpp
plugboard.o: plugboard.cpp plugboard.h
	g++ -Wall -g -c plugboard.cpp
helper.o: helper.cpp helper.h
	g++ -Wall -g -c helper.cpp

clean:
	rm -f *.o execute enigma







