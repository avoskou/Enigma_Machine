#include <iostream>
#include <fstream>
#include "errors.h"

using namespace std;

/*******general use**********************************************/


void open_file(ifstream &in_stream,char* in_file){
  in_stream.open(in_file);
  if (in_stream.fail()){
    cout<<"ERROR OPENING CONFIGURATION FILE";
    throw ERROR_OPENING_CONFIGURATION_FILE;
  }
}  

  
void invalid_index(int digit,char* file,const char* object){
  if(digit<0 || digit>25){
    cerr<<object<<" mapping invalid index "<<file<<endl;
    throw INVALID_INDEX;
  }
}

bool scan_numeric(ifstream &in_stream,int &digit,char* file,const char* object){
  if(!(in_stream>>digit)){
    if(!in_stream.eof()){
      cerr<<"Non-numeric character in "<<object<<" file "<<file<<endl; 
      throw NON_NUMERIC_CHARACTER;
    }
    if(in_stream.eof()) return 0;
  }
  return 1;
}


/**********************used in main******************************************************/



void insufficient_number_of_parameters_check(int argc){
   if(argc<=2 || argc==4){
      cerr<<"usage: enigma plugboard-file reflector-file (<rotor-file>* rotor-positions)?"<<endl;
      throw INSUFFICIENT_NUMBER_OF_PARAMETERS ;
   }
}
