#include <iostream>
#include <fstream>
#include <string>
//#include "enigma.h"
#include "errors.h"
#include "plugboard.h"
#include "helper.h"

using namespace std;

bool Plugboard::read_digit(ifstream &in_stream,int &temp,int counter,char* plugboard_file){
  
  if(!(in_stream>>temp)){
     if(in_stream.eof()){
          if(counter%2==1){   
	     cerr<<"Incorrect number of parameters in plugboard file plugboard.pb"<<endl;  //  error for odd total digits readed 
	     throw INCORRECT_NUMBER_OF_PLUGBOARD_PARAMETERS;                               //..........
          }
          return 0;
     }
     cerr<<"Non-numeric character in plugboard file plugboard.pb"<<endl;           //error if reading fail and is no the endof file
     throw NON_NUMERIC_CHARACTER;
  }
  
  invalid_index(temp,plugboard_file,"plugboard");
 
  
  return 1;
}
  


Plugboard::Plugboard(char* plugboard_file){
  int temp1=0,temp2=0;                                             //temporary variables for the input pairs 
  ifstream in_stream;
  open_file(in_stream,plugboard_file);

  for(int i=0;i<=25;i++)                                           // begins by mapping each digit to itself  
    wiring[i]=i;                                                   // ............

  for(int counter=0;read_digit(in_stream,temp2,counter,plugboard_file);counter++){
    if( counter>0 && ( counter%2)){                                           
	if(temp1==temp2 || wiring[temp1]!=temp1 || wiring[temp2]!=temp2 ){
	  cerr<<"imposible_plugboard_configuaration";
	  throw IMPOSSIBLE_PLUGBOARD_CONFIGURATION;
	}
	wiring[temp1]=temp2;                                       //  fchange  maping of  the given pairs
	wiring[temp2]=temp1;                                       //  change  maping of  the given pairs
    }
      temp1=temp2;
  }
  in_stream.close();
}


void Plugboard::map(int &digit){         
  digit=wiring[digit];                                           // returns the mapping if it exist else  just itself        
}  
