#include <iostream>
#include <fstream>
#include <string>
#include "enigma.h"
#include "errors.h"
#include "helper.h"

using namespace std;

void Enigma::starting_position_errors(ifstream &in_stream,int i,char* file){
  if(in_stream.eof()){
     cerr<<"No starting position for rotor "<<i<<" in rotor position file: "<< file<<endl;
     throw NO_ROTOR_STARTING_POSITION;
  }
}

  
Enigma::Enigma(int argc,char** argv){
  
  int initial_rot,i=0;
  ifstream in_stream;    
  num_rotors=argc-4;
  plugboard= new Plugboard(argv[1]);
  reflector= new Reflector(argv[2]);
  if(num_rotors>0)
    rotor = new Rotor[num_rotors];
  else
    rotor= new Rotor;
  
  open_file(in_stream,argv[argc-1]);
  
  for(i=0;i<num_rotors;i++){
    
    scan_numeric(in_stream,initial_rot,argv[argc-1],"rotor position");
    invalid_index(num_rotors,argv[argc-1],"rotor position");
    if(i==0){
      rotor[i].setup(argv[3],initial_rot);
    }
    else{
      rotor[i].setup(argv[3+i],initial_rot,rotor[i-1]);
    }
    starting_position_errors(in_stream,i,argv[argc-1]);
    
  }
  scan_numeric(in_stream,initial_rot,argv[argc-1],"rotor position");
  in_stream.close();
}




Enigma::~Enigma(){
  if(num_rotors>0)
    delete [] rotor;
  else
    delete rotor;
  delete plugboard;
  delete reflector;
}



int Enigma::encrypt(int digit){
  
  if(num_rotors!=0)                       // if there is at least one rotor rottate the first
    rotor[num_rotors-1].rotate();
  
  plugboard->map(digit);             // plugboard initial mapping
 
  for(int i=(num_rotors-1);i>=0;i--)      // passing throughout rotors right to left  
    rotor[i].map_fw(digit); 
  
  reflector->reflect(digit);              // reflect mapping
  
  for(int i=0;i<num_rotors;i++)           // passing throughout rotors left to right
    rotor[i].map_bw(digit);

  plugboard->map(digit);             //plugboard  final mapping    

  return digit;                           //returns final encrypted digit
}   



