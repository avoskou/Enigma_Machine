
#include <iostream>
#include <fstream>
#include "rotors.h"
#include "plugboard.h"
#include "reflector.h"
//#include "enigma.h"

using namespace std;


class Enigma{
  private:
    int num_rotors;           //number of rotors
    Rotor* rotor;             //pointer to the rotor or rotors array
    Plugboard *plugboard;     //pointer to the plugboard
    Reflector *reflector;     //pointer to the reflector
    
    //checks if there is wrong number of digit in the position file
    void starting_position_errors(ifstream &in_stream,int i,char* file);

  public:
    //constract using the given files
    Enigma(int argc,char** argv);
    
    //delete the dynamic memory/the parts of the enigma
    virtual ~Enigma();
    
    //main encryption procedure 
    int encrypt(int digit);
    
};
